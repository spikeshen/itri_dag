

# Using SNOPT

## Reference Site
> http://ccom.ucsd.edu/~optimizers/usage/cpp/

## Compile SNOPT
```sh
g++ -O -I$SNOPT/include -c myProb.cpp -o myProb.o  
g++ -O myProb.o  -o sntoya -Wl,-rpath $SNOPT/lib -L$SNOPT/lib -lsnopt7_cpp -lsnopt7
```


#ifndef GRAPH_GRAPH_HPP
#define GRAPH_GRAPH_HPP
#include <cstddef>
#include <vector>
#include <numeric>
#include <stdexcept>
#include <algorithm>
#include <map>
#include <iostream>


namespace dag {
	std::vector<int> sort_indexes(const std::vector<int> &v) {
        // initialize original index locations
        std::vector<int> idx(v.size());
        std::iota(idx.begin(), idx.end(), 0);

        // sort indexes based on comparing values in v
        std::sort(idx.begin(), idx.end(),
            [&v](int i1, int i2) {return v[i1] > v[i2];});

        return idx;
    }

    template <typename W, typename V = std::size_t>
    class edge {
        V v;
        V w;
        W wght;

    public:
        edge(const V& pfrom, const V& pto, const W& pweight)
        : v(pfrom), w(pto), wght(pweight) { }

        edge(V&& pfrom, V&& pto, W&& pweight)
        : v(std::move(pfrom)), 
          w(std::move(pto)), 
          wght(std::move(pweight)) { }

        V from() const { return v; }
        V to() const { return w; }
        const W& weight() const { return wght; }
    };

    template<typename W>
    class graph {
// W = std::pair<int, int>
    public:
        using adj_structure = std::vector<edge<W>>;

        graph(std::size_t V)
            : adj_list(std::vector<adj_structure>(V, std::vector<edge<W>>())) { }

        graph(std::size_t V, std::initializer_list<edge<W>> list) 
            : graph(V) 
        {
            for (size_t i = 0; i < V; ++i)
                rank.push_back(0);
            for (const auto &e : list)
                add_edge(e);
        }

        void add_edge(const edge<W>& e) {
            check(e.from());
            check(e.to());
            adj_list[e.from()].push_back(e);

            //TODO:: Randomly insert node (currently insert by rank order
            if (rank[e.to()] != rank[e.from()] + 1)
                rank[e.to()] = rank[e.from()] + 1;

            fromlist[e.to()].push_back(e.from());
            tolist[e.from()].push_back(e.to());
            ++edges;
        }

        const adj_structure& adj(std::size_t v) const {
            return adj_list[v];
        }

        std::size_t E() const {
            return edges;
        }

        std::size_t V() const {
            return adj_list.size();
        }

        auto Get_Rank(int idx) {
            return rank[idx];
        }

        auto MaxRank() const {
            return *std::max_element(rank.begin(), rank.end());
        }

        void Print_Rank () {
            for (auto it = rank.begin(); it < rank.end(); ++it) {
                std::cout << "Node " << std::distance(rank.begin(), it);
                std::cout << ": " << *it << std::endl;
            }
        }
        
        friend std::vector<int> sort_indexes(const std::vector<int> &v);

		std::vector<int> Sort_indexes() {
            return sort_indexes(rank);
        }

		std::vector<int> Get_fromlist(int vertex) {
            if (rank[vertex] == 0)
                fromlist[vertex].push_back(vertex);
            return fromlist[vertex];
        }

		std::vector<int> Get_tolist(int vertex) {
            if (rank[vertex] == MaxRank())
                tolist[vertex].push_back(vertex);
            return tolist[vertex];
        }

    private:
        std::vector<adj_structure>  adj_list;
        std::size_t edges = 0;
        std::vector<int> rank;
        std::map<int, std::vector<int> > fromlist;
        std::map<int, std::vector<int> > tolist;

        void check(std::size_t v) const {
            if (v >= V()) {
                throw std::out_of_range("vertex out of range");
            }
        }
        
    };

}
#endif // GRAPH_GRAPH_HPP

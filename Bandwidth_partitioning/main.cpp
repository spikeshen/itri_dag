#include <vector>
#include <iostream>
#include <map>
#include <utility> // make_pair
#include <type_traits>
#include <cstdlib>
#include <chrono>
#include <ctime>
#include "../Graph/Graph.hpp"
#include "BW_partitioning_alg.hpp"

int main(void)
{
	srand (std::chrono::system_clock::now().time_since_epoch().count());
	using Vertex = typename size_t;
	using Bandwidth = typename double;
	using Link = typename std::pair<Vertex, Vertex>;
	using LinkProperty = typename std::pair<int, int>;

	auto num_sessions = 20;
	// { src, dst, { latency, bandwidth}}
	dag::graph<LinkProperty> g(6,
    {
        { 0, 1, { 1, 16000  } },
        { 0, 2, { 1, 17000  } },
        { 1, 3, { 1, 18000  } },
        { 1, 4, { 1, 15000  } },
        { 2, 3, { 1, 13000  } },
        { 2, 4, { 1, 12000  } },
        { 3, 5, { 1, 11000  } },
        { 4, 5, { 1, 20000  } }
    });
    dag::BW_Info<Vertex, Bandwidth, LinkProperty> bw_info(g, num_sessions);
	dag::BW_Partitioning_Alg<LinkProperty, Vertex, Bandwidth>(g, bw_info, num_sessions);
    
	std::cin.get();
    return 0;
}


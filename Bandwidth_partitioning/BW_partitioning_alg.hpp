#ifndef BW_PARTITIONING_ALG
#define BW_PARTITIONING_ALG

#include <map>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include "../Graph/Graph.hpp"

namespace dag {
    template < typename T = int, typename W = double, typename S = std::pair<int, int> >
    class Session_Info {
		using Vertex = typename T;
		using Bandwidth = typename W;
		using Link = typename std::pair<Vertex, Vertex>;
		using LinkProperty = typename S;
    private:
        std::map<Link, Bandwidth> outgoing_partition; //o(u, v)
        std::map<Link, Bandwidth> incoming_partition; //b(v, u)
        std::map<Link, Bandwidth> avg_link_packet_bw; //a(v, u) default 1200
        std::map<Link, Bandwidth> max_flow_bw;        //m(v, u)
        std::map<Vertex, Bandwidth>  packet_num;      //count

    public:
        size_t allpackets;
    
        Session_Info () {
            // std::cout << "Session_Info Constructor Activated\n";
        }

		Session_Info (graph<LinkProperty> &g) {
			// std::cout << "Session_Info Constructor Activated\n";
		}
        virtual ~Session_Info () {}


		Bandwidth get_outgoing_partition (Link &link) {
            if (outgoing_partition.find(link) != outgoing_partition.end())
                return outgoing_partition[link];
            else {
                outgoing_partition[link] = -1;
                return -1; //get outgoing_partition from graph
            }
        }

		Bandwidth get_incoming_partition (Link &link) {
            if (incoming_partition.find(link) != incoming_partition.end())
                return incoming_partition[link];
            else {
                incoming_partition[link] = 0;
                return 0;
            }
        }

		Bandwidth get_max_flow_bw (Link &link) {
            if (max_flow_bw.find(link) != max_flow_bw.end())
                return max_flow_bw[link];
            else {
                max_flow_bw[link] = 0;
                return 0;
            }
        }
        
		Bandwidth get_avg_link_packet_bw (Link &link) {
            if (avg_link_packet_bw.find(link) != avg_link_packet_bw.end())
                return avg_link_packet_bw[link];
            else {
                avg_link_packet_bw[link] = 0;
                return 0;
            }
        }

        Vertex get_packet_num (Vertex vertex) {
            if (packet_num.find(vertex) != packet_num.end())
                return packet_num[vertex];
            else {
                packet_num[vertex] = 0;
                return 0;
            }
        }

        void set_outgoing_partition (Vertex u, Vertex v, Bandwidth part) { //(u,v) in Q
            outgoing_partition[Link(u, v)] = part;
        }
        void set_incoming_partition (Vertex v, Vertex u, Bandwidth part) { //(v,u) in P
            incoming_partition[Link(v, u)] = part;
        }
        void set_avg_link_packet_bw (Vertex v, Vertex u, Bandwidth bw) {
            avg_link_packet_bw[Link(v, u)] = bw;
        }
        void set_max_flow_bw (Vertex v, Vertex u, Bandwidth max_bw) {
            max_flow_bw[Link(v, u)] = max_bw;
        }
        void set_packet_num (Vertex u, Bandwidth packets_bw) {
            packet_num[u] = packets_bw;
        }

		//void set_available_bandwidth(graph<LinkProperty> &g) {
		//	for (auto v : g.Sort_indexes()) {
		//		for (auto &e : g.adj(v)) {
		//			Link ilink(e.from(), e.to());
		//			Bandwidth incoming_bw = get_incoming_partition(ilink);
		//			Bandwidth total_bw = e.weight().second;
		//			set_available_link_bandwidth(ilink, total_bw - incoming_bw);

		//			if (total_bw - incoming_bw < 0) {
		//				std::cerr << "Total bandwidth is: " << total_bw;
		//				std::cerr << ", while incoming link bandwidth uses: " << incoming_bw;
		//			}
		//		}
		//	}
		//}
    };

    
	template < typename T = int, typename W = double, typename S = std::pair<int, int> >
	class BW_Info {
		using Vertex = typename T;
		using Bandwidth = typename W;
		using Link = typename std::pair<Vertex, Vertex>;
		using LinkProperty = typename S;
	private:
		std::map<Link, Bandwidth> available_bw;
		std::map<Link, Bandwidth> flow;
		std::vector< Session_Info<T, W, LinkProperty> > sessions;
	public:
		BW_Info (graph<LinkProperty> &g, size_t num_sessions) {
			Session_Info<Vertex, Bandwidth, LinkProperty> _session;
			for (auto v : g.Sort_indexes()) {
				for (auto &e : g.adj(v)) {
					//Session initial
					_session.set_avg_link_packet_bw(e.from(), e.to(), 1200);

					//BW info initial
					Link ilink(e.from(), e.to()); //current incoming link
					set_available_link_bandwidth(ilink, e.weight().second);
				}
			}
			sessions = std::vector< Session_Info<Vertex, Bandwidth, LinkProperty> >(num_sessions, _session);

			for (auto &s : sessions) {
				auto allpackets = rand() % (1200 - 1000 + 1) + 1000; // 1000 ~ 1200
				s.allpackets = allpackets;
			}

			
		}

		virtual ~BW_Info() {}

		Session_Info<Vertex, Bandwidth, LinkProperty>& Get_Session(const size_t session_num) {
			return sessions.at(session_num);
		}

		Bandwidth get_available_bandwidth(Link &link) {
			return available_bw[link];
		}

		Bandwidth get_flow(Link &link) {
			return flow[link];
		}

		void set_available_link_bandwidth(Link &link, Bandwidth bw) {
			available_bw[link] = bw;
		}

		void set_flow(Link &link, Bandwidth bw) {
			flow[link] = bw;
		}
	};

	template <typename T = int, typename W = double>
    void CalculateBackPressure (std::vector<W> &incoming_partition_vec,
                                std::vector<W> &outgoing_partition_vec,
                                const T target_idx,
                                W &avg_link_packet_bw,
                                const W maxBW) {
        W incoming_bw_sum = 0.0;
        auto outgoing_max = *std::max_element(outgoing_partition_vec.begin(),
                                              outgoing_partition_vec.end());
        auto min_of_avg_out = std::min(outgoing_max, avg_link_packet_bw);

        for (auto &e : incoming_partition_vec)
            incoming_bw_sum += e;
        incoming_bw_sum -= incoming_partition_vec[target_idx]; // exclude obj partition
        
        // solution
        incoming_partition_vec[target_idx] = (min_of_avg_out + 
                                              outgoing_max + 
                                              maxBW - 
                                              2*incoming_bw_sum) / 3;
    }


	template <typename S = std::pair<int, int>, typename T = int, typename U = double>
	void BackPressureAlg(graph<S> & g, BW_Info<T, U, S> & bw_info, size_t num_session) {
		using Link = typename std::pair<T, T>;
		using Vertex = typename int;
		using Bandwidth = typename U;

		auto max_rank = g.MaxRank();
		for (auto i = 0; i < num_session; ++i) {
			for (auto v : g.Sort_indexes()) {
				for (auto &e : g.adj(v)) {
					std::cout << "Vertex " << e.to() << ": ";
					std::cout << e.from() << "->";
					std::cout << e.to() << std::endl;

					Link ilink(e.from(), e.to()); //current incoming link
					std::vector<Bandwidth> incoming_partition_vec;
					std::vector<Bandwidth> outgoing_partition_vec;
					Vertex target_idx;
					Bandwidth avg_link_packet_bw = bw_info.Get_Session(i).get_avg_link_packet_bw(ilink);
					Bandwidth max_ilink_bw = e.weight().second;

					for (auto &i : g.Get_fromlist(e.to())) {
						if (i == e.from()) {
							target_idx = incoming_partition_vec.size();
						}
						Link _ilink(i, e.to());
						incoming_partition_vec.push_back(bw_info.Get_Session(i).get_incoming_partition(_ilink));
					}

					for (auto &o : g.Get_tolist(e.to())) {
						if (o == e.to()) {
							outgoing_partition_vec.push_back(bw_info.Get_Session(i).allpackets);
							break;
						}
						Link _olink(e.to(), o);
						outgoing_partition_vec.push_back(bw_info.Get_Session(i).get_outgoing_partition(_olink));
					}

					CalculateBackPressure<Vertex, Bandwidth>(
						incoming_partition_vec,
						outgoing_partition_vec,
						target_idx,
						avg_link_packet_bw,
						max_ilink_bw);

					if (incoming_partition_vec[target_idx] < 0) {
						std::cerr << "[Error] Incoming link bandwidth usage is lower than 0!!\n";
						std::cerr << "[Error] Incoming partition:\n";
						for (auto ip : incoming_partition_vec)
							std::cerr << "\t" << ip << std::endl;
						std::cerr << "[Error] Outgoing partition:\n";
						for (auto op : outgoing_partition_vec)
							std::cerr << "\t" << op << std::endl;
						std::cerr << "[Error] Target partition: \n\t" << incoming_partition_vec[target_idx] << "\n";
						std::cerr << "[Error] Maximum incoming link bandwidth:\n\t" << max_ilink_bw << "\n";
						std::cerr << "[Error] Average incoming link packets' bandwidth\n\t" << avg_link_packet_bw << "\n";
						std::cerr << "[Error] Forced to 0\n";
						incoming_partition_vec[target_idx] = 0;
					}

					bw_info.Get_Session(i).set_outgoing_partition(e.from(), e.to(), incoming_partition_vec[target_idx]);
					bw_info.Get_Session(i).set_incoming_partition(e.from(), e.to(), incoming_partition_vec[target_idx]);

					std::cout << incoming_partition_vec[target_idx] << std::endl;
				}
			}
		}
	}

	template <typename S = std::pair<int, int>, typename T = int, typename U = double>
	void UpdatePartitionInfo(graph<S> & g, BW_Info<T, U, S> & bw_info, size_t num_session) {
		using Link = typename std::pair<T, T>;
		using Vertex = typename int;
		using Bandwidth = typename U;

		for (auto v : g.Sort_indexes()) {
			for (auto &e : g.adj(v)) {
				Link ilink(e.from(), e.to()); //current incoming link
				Bandwidth remainBandwidth = bw_info.get_available_bandwidth(ilink);
				for (auto i = 0; i < num_session; ++i) {
					if (remainBandwidth >= 0) {
						Bandwidth outgoing_max = 0;
						Bandwidth packet_num = 0;
						Bandwidth flow = 0;

						for (auto &i : g.Get_fromlist(e.to())) {
							Link _ilink(i, e.to());
							packet_num += bw_info.Get_Session(i).get_incoming_partition(_ilink);
						}
						bw_info.Get_Session(i).set_packet_num(e.from(), packet_num);

						for (auto &o : g.Get_tolist(e.to())) {
							if (o == e.to()) {
								break;
							}
							Link _olink(e.to(), o);
							Bandwidth _o_bw = bw_info.Get_Session(i).get_outgoing_partition(_olink);
							outgoing_max = (_o_bw > outgoing_max) ? _o_bw : outgoing_max;
						}
						flow = std::min({ outgoing_max, packet_num, remainBandwidth });
						bw_info.set_flow(ilink, flow);
						bw_info.set_available_link_bandwidth(ilink, remainBandwidth - flow);
					}
					else {
						bw_info.set_flow(ilink, 0);
					}
				}
			}
		}
	}

	template <typename S = std::pair<int, int>, typename T = int, typename U = double>
	void BW_Partitioning_Alg(graph<S> & g, BW_Info<T, U, S> & bw_info, size_t num_session) {
		using Link = typename std::pair<T, T>;
		using Vertex = typename int;
		using Bandwidth = typename U;

		BackPressureAlg(g, bw_info, num_session);
		UpdatePartitionInfo(g, bw_info, num_session);
	}
}

#endif

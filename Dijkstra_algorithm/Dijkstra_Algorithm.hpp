/*Dijkstra's algorithm with Fibonacci heap*/
// ref: https://codereview.stackexchange.com/questions/138647/dijkstras-single-source-shortest-path-algorithm-with-fibonacci-heap
#ifndef DIJKSTRA_ALGORITHM_HPP
#define DIJKSTRA_ALGORITHM_HPP

#include "../Graph/Graph.hpp"
#include <limits>
#include "index_fib_heap.hpp"
#include <type_traits>
#include <functional>
#include <deque>
#include <climits>

namespace dag {


    struct identity {
        template<typename U>
        constexpr auto operator()(U&& v) const noexcept
            -> decltype(std::forward<U>(v)) {
            return std::forward<U>(v);
        }
    };   

    template<typename W, 
             typename F = identity, 
             typename IPQ = dag::index_fib_heap<
                                typename std::remove_reference<
                                    decltype(F()(std::declval<W>()))
                                >::type
                            >
            >
    class dijkstra_algorithm {

        using Weight = typename std::remove_reference<
            decltype(F()(std::declval<W>()))
        >::type;

        F               map_w;
        IPQ             pq;
        const graph<W>  *g;
        const edge<W>   **edge_to;
        Weight          *distances;

        std::pair<std::size_t, Weight> next() {
            if (pq.empty()) {
                return { SIZE_MAX, { } };
            }
            auto min = pq.top();
            pq.pop();
            for (const auto &e : g->adj(min.first)) {
                relax(&e);
            }
            return min;
        }

        void relax(const edge<W>* edge) {
            auto to    = edge->to();
            auto new_d = distances[edge->from()]
                            + map_w(edge->weight());

            if (new_d < distances[to]) {
                distances[to] = new_d;
                edge_to[to]   = edge;
                if (!pq.insert(to, new_d)) {
                    pq.decrease(to, new_d);
                }    
            }
        }

        void check(std::size_t v) const {
            if (g == nullptr || v >= g->V()) {
                throw std::out_of_range("vertex out of range");
            }
        }

    public:

        dijkstra_algorithm(std::size_t source, const graph<W>* pg, F pmap_w, IPQ ppq) 
            : g(pg), 
                map_w(pmap_w), 
                edge_to(new const edge<W>*[pg->V()]()),
                distances(new Weight[pg->V()]),
                pq(std::move(ppq)) 
        {       
            for (std::size_t i = 0; i < pg->V(); ++i) {
                distances[i] = std::numeric_limits<Weight>::max();
            }
            pq.insert(source, distances[source] = { });   
        }

        dijkstra_algorithm(std::size_t source, const graph<W>* pg, F pmap_w)
            : dijkstra_algorithm(source, pg, pmap_w, { pg->V() }) { }

        dijkstra_algorithm(std::size_t source, const graph<W>* pg)
            : dijkstra_algorithm(source, pg, { }) { }

        dijkstra_algorithm(dijkstra_algorithm<W, F, IPQ> && dijkstra) noexcept
            : map_w(std::move(dijkstra.map_w)), 
            pq(std::move(dijkstra.pq)), 
            g(dijkstra.g), 
            edge_to(dijkstra.edge_to), 
            distances(dijkstra.distances) 
        { 
            dijkstra.g         = nullptr;
            dijkstra.edge_to   = nullptr;
            dijkstra.distances = nullptr;
        }

        dijkstra_algorithm(const dijkstra_algorithm<W, F, IPQ>& dijkstra)
            : map_w(dijkstra.map_w),
                pq(dijkstra.pq),
                g(dijkstra.g) 
        {
            edge_to   = new const edge<W>*[g->V()];
            distances = new Weight[g->V()];
            for (std::size_t i = 0; i < g->V(); ++i) {
                edge_to[i]   = dijkstra.edge_to[i];
                distances[i] = dijkstra.distances[i];
            }
        }

        ~dijkstra_algorithm() {       
            delete[] edge_to;
            delete[] distances;
        }

        Weight distance_to(std::size_t v) const {
            check(v);
            return distances[v];
        }

        bool has_path(std::size_t v) const {
            return distance_to(v) < std::numeric_limits<Weight>::max();
        }

        std::deque<const edge<W>*> path(std::size_t v) const {
            check(v);

            std::deque<const edge<W>*> path;
            auto e = edge_to[v];
            while (e != nullptr) {
                path.push_front(e);
                e = edge_to[e->from()];
            }
            return path;
        }

        friend void swap(dijkstra_algorithm<W, F, IPQ>& first, 
                            dijkstra_algorithm<W, F, IPQ>& second) noexcept {
            using std::swap;
            swap(first.map_w, second.map_w);
            swap(first.pq, second.pq);
            swap(first.g, second.g);
            swap(first.edge_to, second.edge_to);
            swap(first.distances, second.distances);

        }

        dijkstra_algorithm<W, F, IPQ>& operator=(dijkstra_algorithm<W, F, IPQ> other)  noexcept {
            swap(*this, other);
            return *this;
        }

        class dijkstra_iterator
            : public std::iterator<std::forward_iterator_tag,
            std::remove_cv_t<std::size_t>,
            std::ptrdiff_t,
            std::size_t*,
            std::size_t& >
        {

            dijkstra_algorithm<W, F, IPQ>*  dijkstra;
            std::pair<std::size_t, Weight> current;

        public:

            explicit dijkstra_iterator(dijkstra_algorithm<W, F, IPQ>*  pdijkstra)
                : dijkstra(pdijkstra) { 
                current = pdijkstra->next();

            }

            explicit dijkstra_iterator(std::size_t pcurrent)
                : current({ pcurrent, { } }) { }

            dijkstra_iterator& operator++ () {
                current = dijkstra->next();
                return *this;
            }
            dijkstra_iterator operator++ (int) {
                dijkstra_iterator tmp(*this);
                current = dijkstra->next();
                return tmp;
            }

            bool operator == (const dijkstra_iterator& rhs) const {
                return current.first == rhs.current.first;
            }

            bool operator != (const dijkstra_iterator& rhs) const {
                return current.first != rhs.current.first;
            }

            const std::pair<std::size_t, Weight>& operator* () const {
                return current;
            }

            const std::pair<std::size_t, Weight>&  operator-> () const {
                return current;
            }
        };

        using iterator = dijkstra_iterator;
        iterator begin() {
            return iterator(this);
        }

        iterator end()  {
            return iterator(SIZE_MAX);
        }
    };
}

#endif // DIJKSTRA_ALGORITHM_HPP
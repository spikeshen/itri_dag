#!python3
import pulp

bp_problem = pulp.LpProblem("Back pressure", pulp.LpMinimize)

a = 1000
maxj = 300
m = 10000

# Variables

A = pulp.LpVariable('A', lowBound=0, cat='Continuous')
B = pulp.LpVariable('B', lowBound=0, cat='Continuous')
C = pulp.LpVariable('C', lowBound=0, cat='Continuous')
D = pulp.LpVariable('D', lowBound=0, cat='Continuous')
E = pulp.LpVariable('E', lowBound=0, cat='Continuous')
F = pulp.LpVariable('F', lowBound=0, cat='Continuous')
G = pulp.LpVariable('G', lowBound=0, cat='Continuous')
H = pulp.LpVariable('H', lowBound=0, cat='Continuous')

# Function & Solve
bp_problem += 2*H - 2*min(1000, 1200) - 2*max(1200, 1200) - 2 * m + 4 * (H + 0)
bp_problem += 2*H - 2*min(1000, 1200) - 2*max(1200, 1200) - 2 * m + 4 * (H + 0) >= 0
bp_problem.solve()

bp_problem += 2*G - 2*min(1000, 1200) - 2*max(1200, 1200) - 2 * m + 4 * (H.varValue + G)
bp_problem += 2*G - 2*min(1000, 1200) - 2*max(1200, 1200) - 2 * m + 4 * (H.varValue + G) >= 0
bp_problem.solve()

bp_problem += 2*C - 2*min(a, G.varValue) - 2*max(G.varValue, 0) - 2 * m + 4 * (C + 0)
bp_problem += 2*C - 2*min(a, G.varValue) - 2*max(G.varValue, 0) - 2 * m + 4 * (C + 0) >= 0
bp_problem.solve()

bp_problem += 2*E - 2*min(a, G.varValue) - 2*max(G.varValue, 0) - 2 * m + 4 * (C.varValue + E)
bp_problem += 2*E - 2*min(a, G.varValue) - 2*max(G.varValue, 0) - 2 * m + 4 * (C.varValue + E) >= 0
bp_problem.solve()

bp_problem += 2*D - 2*min(a, H.varValue) - 2*max(H.varValue, 0) - 2 * m + 4 * (D + 0)
bp_problem += 2*D - 2*min(a, H.varValue) - 2*max(H.varValue, 0) - 2 * m + 4 * (D + 0) >= 0
bp_problem.solve()

bp_problem += 2*F - 2*min(a, H.varValue) - 2*max(H.varValue, 0) - 2 * m + 4 * (D.varValue + F)
bp_problem += 2*F - 2*min(a, H.varValue) - 2*max(H.varValue, 0) - 2 * m + 4 * (D.varValue + F) >= 0
bp_problem.solve()

bp_problem += 2*A - 2*min(a, max(C.varValue, D.varValue)) - 2*max(C.varValue, D.varValue) - 2 * m + 4 * (A)
bp_problem += 2*A - 2*min(a, max(C.varValue, D.varValue)) - 2*max(C.varValue, D.varValue) - 2 * m + 4 * (A) >= 0
bp_problem.solve()

bp_problem += 2*B - 2*min(a, max(E.varValue, F.varValue)) - 2*max(E.varValue, F.varValue) - 2 * m + 4 * (B)
bp_problem += 2*B - 2*min(a, max(E.varValue, F.varValue)) - 2*max(E.varValue, F.varValue) - 2 * m + 4 * (B) >= 0
bp_problem.solve()

for variable in bp_problem.variables():
    print("{}={}".format(variable.name, variable.varValue))
